const ItemKey = (props) => {
    return (
        <li className="key" id={props.id}>
            {props.key_open}
        </li>
    )
}

export default ItemKey