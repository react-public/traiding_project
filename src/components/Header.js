import React, {Fragment, useContext} from 'react'
import {NavLink} from 'react-router-dom'
import AuthContext from '../context/AuthContext'

const Header = () => {
    let {user, logoutUser} = useContext(AuthContext)

    let headerAuthorized = (
        <Fragment>
            <p onClick={logoutUser}>Выход</p>
            <span> | </span>
            <NavLink to={'/prifile/admission'}>Доступы</NavLink>
        </Fragment>
    )

    let headerNotAuthorized = (
        <Fragment>
            <NavLink to={'/login'}>Вход</NavLink>
        </Fragment>
    )


    return (
        <header>
            <NavLink to={'/'}>Главная</NavLink>
            <span> | </span>
            <NavLink to={'/manual'}>Инструкция</NavLink>
            <span> | </span>
            {user ? headerAuthorized : headerNotAuthorized}
        </header>
    )
}

export default Header