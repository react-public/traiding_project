import {Outlet} from "react-router-dom";
import React, {Fragment} from "react";
import Header from './Header'

const Layout = () => {
    return (
        <Fragment>
            <Header/>

            <Outlet />

            <footer>
                2023
            </footer>
        </Fragment>
    )
}

export default Layout