import {Fragment, useContext, useEffect, useState} from "react";
import AuthContext from "../context/AuthContext";
import ItemKey from "./ItemKey";

const ListKeysBinance = () => {
    const {authTokens, logoutUser} = useContext(AuthContext);
    let [listKeys, setListKeys] = useState([])

    useEffect(() => {
        getListKeysBinance()
    }, [])

    const getListKeysBinance = async () => {
        let response = await fetch('http://127.0.0.1:8000/api/v1/market/binancef', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + String(authTokens.access)
            }
        })
        let data = await response.json()
        console.log(data)
        if (response.status === 200) {
            setListKeys(data)
        } else if (response.statusText === 'Unauthorized') {
            logoutUser()
        }
        listKeys.map((keys) => (console.log(keys)))
    }

    return (
        <Fragment>
            <ul className="ListKeysBinance">
                {listKeys.map((keys) => (
                    <ItemKey
                        key={keys.id}
                        id={keys.id}
                        key_open={keys.key}
                    />
                ))}
            </ul>
        </Fragment>
    )
}

export default ListKeysBinance