import React, {Fragment} from 'react'
import {Route, Routes} from 'react-router-dom'
import {AuthProvider} from './context/AuthContext'

import HomePage from './pages/HomePage'
import LoginPage from './pages/LoginPage'
import ProfileListKeys from './pages/ProfileListKeys'
import NotFoundPage from './pages/NotFoundPage'
import axios from "axios";

import PrivateRoute from './utils/PrivateRoute'
import Layout from "./components/Layout";

// class App extends React.Component {
//     state = {details: []}
//
//     componentDidMount() {
//         let data;
//         axios.get('http://localhost:8080/api/v1/market/binancef/').then(res => {
//             data = res.data;
//             this.setState({
//                 details: data
//             });
//         }).catch(err => console.log(err));
//     }


function App() {
    return (
        <Fragment>
            <AuthProvider>
                <Routes>
                    <Route path={'/'} element={<Layout/>}>
                        <Route index element={
                            <PrivateRoute>
                                <HomePage/>
                            </PrivateRoute>
                        }/>
                        <Route path={'/login'} element={<LoginPage/>}/>
                        <Route path={'/prifile/admission'} element={
                            <PrivateRoute>
                                <ProfileListKeys/>
                            </PrivateRoute>
                        }/>
                        <Route path={'*'} element={<NotFoundPage/>}/>
                    </Route>
                </Routes>
            </AuthProvider>
        </Fragment>
    );
}

export default App;
